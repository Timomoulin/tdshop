<?php
// idArticle	nom	description	prixUnitaire	estDisponible	idCategorie	estFragile
class Article{
    private int $idArticle;
    private string $nom;
    private ?string $description;
    private float $prixUnitaire;
    private bool $estDisponible;
    private int $idCategorie;
    private bool $estFragile;

  /**
   * Permet de valorisé les attributs d'un objet
   *
   * @param string $nom
   * @param string $description
   * @param float $prixUnitaire
   * @param boolean $estDisponible
   * @param boolean $estFragile
   * @param integer $idCategorie
   * @return void
   */
    public function hydrate(string $nom,string $description,float $prixUnitaire,bool $estDisponible,bool $estFragile,int $idCategorie){
        $this->setNom($nom);
        $this->setDescription($description);
        $this->setPrixUnitaire($prixUnitaire);
        $this->setEstDisponible($estDisponible);
        $this->setEstFragile($estFragile);
        $this->setIdCategorie($idCategorie);
    }

    /**
     * Get the value of idArticle
     *
     * @return  int
     */
    public function getIdArticle()
    {
        return $this->idArticle;
    }

    /**
     * Set the value of idArticle
     *
     * @param  int  $idArticle
     *
     * @return  self
     */
    public function setIdArticle(int $idArticle)
    {
        if($idArticle>0)
        {
            $this->idArticle = $idArticle;
        }
        

        return $this;
    }

    /**
     * Get the value of nom
     *
     * @return  string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set the value of nom
     *
     * @param  string  $nom
     *
     * @return  self
     */
    public function setNom(string $nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get the value of description
     *
     * @return  ?string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of description
     *
     * @param  ?string  $description
     *
     * @return  self
     */
    public function setDescription(?string $description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of prixUnitaire
     *
     * @return  float
     */
    public function getPrixUnitaire()
    {
        return $this->prixUnitaire;
    }

    /**
     * Set the value of prixUnitaire
     *
     * @param  float  $prixUnitaire
     *
     * @return  self
     */
    public function setPrixUnitaire(float $prixUnitaire)
    {
        if($prixUnitaire>0)
        {
             $this->prixUnitaire = $prixUnitaire;
        }
       

        return $this;
    }

    /**
     * Get the value of estDisponible
     *
     * @return  bool
     */
    public function isEstDisponible()
    {
        return $this->estDisponible;
    }

    /**
     * Set the value of estDisponible
     *
     * @param  bool  $estDisponible
     *
     * @return  self
     */
    public function setEstDisponible(bool $estDisponible)
    {
        $this->estDisponible = $estDisponible;

        return $this;
    }

    /**
     * Get the value of idCategorie
     *
     * @return  int
     */
    public function getIdCategorie()
    {
        return $this->idCategorie;
    }

    /**
     * Set the value of idCategorie
     *
     * @param  int  $idCategorie
     *
     * @return  self
     */
    public function setIdCategorie(int $idCategorie)
    {
        $this->idCategorie = $idCategorie;

        return $this;
    }

    /**
     * Get the value of estFragile
     *
     * @return  bool
     */
    public function isEstFragile()
    {
        return $this->estFragile;
    }

    /**
     * Set the value of estFragile
     *
     * @param  bool  $estFragile
     *
     * @return  self
     */
    public function setEstFragile(bool $estFragile)
    {
        $this->estFragile = $estFragile;

        return $this;
    }
}
