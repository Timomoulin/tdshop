<?php
class Categorie{
    private int $idCategorie;
    private string $nom;


    /**
     * Get the value of idCategorie
     *
     * @return  int
     */
    public function getIdCategorie()
    {
        return $this->idCategorie;
    }

    /**
     * Set the value of idCategorie
     *
     * @param  int  $idCategorie
     *
     * @return  self
     */
    public function setIdCategorie(int $idCategorie)
    {
        if($idCategorie>0){
            $this->idCategorie = $idCategorie;
        }
        return $this;
    }

    /**
     * Get the value of nom
     *
     * @return  string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set the value of nom
     *
     * @param  string  $nom
     *
     * @return  self
     */
    public function setNom(string $nom)
    {
        if(strlen($nom)>2){
            $this->nom = $nom;
        }
        return $this;
    }
}