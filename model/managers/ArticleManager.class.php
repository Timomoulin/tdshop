<?php
class ArticleManager{
    private PDO $lePDO;

    public function __construct(PDO $unPDO)
    {
        $this->lePDO=$unPDO;
    }

    public function fetchAllArticle(){

        try {
            $connex=$this->lePDO;
            $sql =$connex->prepare("SELECT * FROM article ORDER BY nom");
            $sql->execute();
            $sql->setFetchMode(PDO::FETCH_CLASS,"Article");
            $resultat = ($sql->fetchAll());
            return $resultat;

        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }

    public function fetchAllArticleByIdCategorie($idCateg){
        try {
            $connex=$this->lePDO;
            $sql =$connex->prepare("SELECT * FROM article where idCategorie=:id");
            $sql->bindValue(":id",$idCateg);
            $sql->execute();
            $sql->setFetchMode(PDO::FETCH_CLASS,"Article");
            $resultat = ($sql->fetchAll());
            return $resultat;

        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }

    public function fetchArticleByIdArticle(int $id){

        try {
            $connex=$this->lePDO;
            $sql =$connex->prepare("SELECT * FROM article where idArticle=:id limit 1");
            $sql->bindValue(":id",$id);
            $sql->execute();
            $sql->setFetchMode(PDO::FETCH_CLASS,"Article");
            $resultat = ($sql->fetch());
            return $resultat;

        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }

    public function createArticle($nom,$description,$prix,bool $estDispo,bool $estFragile,$categ){
        try{
            $connex=$this->lePDO;
            $sql=$connex->prepare("INSERT INTO article (nom,description,prixUnitaire,estDisponible,idCategorie,estFragile) values (:nom,:description,:prix,:estDispo,:idCateg,:estFragile)");
            $sql->bindParam(":nom",$nom);
            $sql->bindParam(":description",$description);
            $sql->bindParam(":prix",$prix);
            $sql->bindParam(":estDispo",$estDispo,PDO::PARAM_BOOL);
            $sql->bindParam(":idCateg",$categ);
            $sql->bindParam(":estFragile",$estFragile,PDO::PARAM_BOOL);
            
            $sql->execute();
            return true;
        }
        catch(PDOException $error){
            echo $error->getMessage();
            return false;
        }
    }

    public function createArticle2(Article $unArticle){
        try{
            $connex=$this->lePDO;
            $sql=$connex->prepare("INSERT INTO article (nom,description,prixUnitaire,estDisponible,idCategorie,estFragile) values (:nom,:description,:prix,:estDispo,:idCateg,:estFragile)");
            $sql->bindValue(":nom",$unArticle->getNom());
            $sql->bindValue(":description",$unArticle->getDescription());
            $sql->bindValue(":prix",$unArticle->getPrixUnitaire());
            $sql->bindValue(":estDispo",$unArticle->isEstDisponible(),PDO::PARAM_BOOL);
            $sql->bindValue(":idCateg",$unArticle->getIdCategorie());
            $sql->bindValue(":estFragile",$unArticle->isEstFragile(),PDO::PARAM_BOOL);
            $sql->execute();
            return true;
        }
        catch(PDOException $error){
            echo $error->getMessage();
            return false;
        }
    }

    public function updateArticle(Article $unArticle){
        try{
            $connex=$this->lePDO;
            $sql=$connex->prepare("UPDATE article set nom=:nom, description=:description, prixUnitaire=:prix,estDisponible=:estDispo,idCategorie=:idCateg, estFragile=:estFragile where idArticle=:id");
            $sql->bindValue(":id",$unArticle->getIdArticle());
            $sql->bindValue(":nom",$unArticle->getNom());
            $sql->bindValue(":description",$unArticle->getDescription());
            $sql->bindValue(":prix",$unArticle->getPrixUnitaire());
            $sql->bindValue(":estDispo",$unArticle->isEstDisponible(),PDO::PARAM_BOOL);
            $sql->bindValue(":idCateg",$unArticle->getIdCategorie());
            $sql->bindValue(":estFragile",$unArticle->isEstFragile(),PDO::PARAM_BOOL);
            $sql->execute();
            return true;
        }
        catch(PDOException $error){
            echo $error->getMessage();
            return false;
        }
    }

    public function deleteArticle(int $idArticle){
        try{
            $connex=$this->lePDO;
            //Activé la transaction
            $connex->beginTransaction();
            //Rien ne se passe tant que le commit est pas executé

            $sql=$connex->prepare("DELETE article_commande FROM article_commande  where idArticle=:id");
            $sql->bindParam(":id",$idArticle);
            $sql->execute();

            $sql1=$connex->prepare("DELETE FROM article where idArticle=:id");
            $sql1->bindParam(":id",$idArticle);
            $sql1->execute();
            //Tout les requetes sont Ok
            $connex->commit();
            return true;
        }
        catch(PDOException $error){
            echo $error->getMessage();
            //Aucunes requetes est executé
            $connex->rollBack();
            return false;
        } 
    
    }
}