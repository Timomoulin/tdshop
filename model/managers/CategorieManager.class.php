<?php
class CategorieManager{
    private PDO $lePDO;

    public function __construct(PDO $unPDO)
    {
        $this->lePDO=$unPDO;
    }

    public function fetchAllCategorie(){

        try {
            $connex=$this->lePDO;
            $sql =$connex->prepare("SELECT * FROM categorie ORDER BY nom");
            $sql->execute();
            $sql->setFetchMode(PDO::FETCH_CLASS,"Categorie");
            $resultat = ($sql->fetchAll());
            return $resultat;

        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }

    public function fetchCategorieById(int $uneId){

        try {
            $connex=$this->lePDO;
            $sql =$connex->prepare("SELECT * FROM categorie where idCategorie=:id ORDER BY nom");
            $sql->bindParam(":id",$uneId);
            $sql->execute();
            $sql->setFetchMode(PDO::FETCH_CLASS,"Categorie");
            $resultat = ($sql->fetch());
            return $resultat;

        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }

    public function createCategorie(string $unNom){
        try{
            $connex=$this->lePDO;
            $sql=$connex->prepare("INSERT INTO categorie values (null,:nom)");
            $sql->bindParam(":nom",$unNom);
            $sql->execute();
            return true;
        }
        catch(PDOException $error){
            echo $error->getMessage();
            return false;
        }
    }

    public function updateCategorie(string $unNom,int $idCategorie){
        try{
            $connex=$this->lePDO;
            $sql=$connex->prepare("UPDATE categorie set nom=:nom where idCategorie=:id");
            $sql->bindParam(":nom",$unNom);
            $sql->bindParam(":id",$idCategorie);
            $sql->execute();
            return true;
        }
        catch(PDOException $error){
            echo $error->getMessage();
            return false;
        }
    }

    public function updateCategorie2(Categorie $objetCategorie){
        try{
            $connex=$this->lePDO;
            $sql=$connex->prepare("UPDATE categorie set nom=:nom where idCategorie=:id");
            $sql->bindValue(":nom",$objetCategorie->getNom());
            $sql->bindValue(":id",$objetCategorie->getIdCategorie());
            $sql->execute();
            return true;
        }
        catch(PDOException $error){
            echo $error->getMessage();
            return false;
        }
    }

    public function deleteCategorie($id){
        try{
            $connex=$this->lePDO;
            $connex->beginTransaction();
            $sql=$connex->prepare("DELETE article_commande FROM article_commande inner join article on article_commande.idArticle=article.idArticle where article.idCategorie=:id");
            $sql->bindParam(":id",$id);
            $sql->execute();

            $sql1=$connex->prepare("DELETE FROM article where idCategorie=:id");
            $sql1->bindParam(":id",$id);
            $sql1->execute();

            $sql2=$connex->prepare("DELETE FROM categorie where idCategorie=:id");
            $sql2->bindParam(":id",$id);
            $sql2->execute();
            $connex->commit();
            return true;
        }
        catch(PDOException $error){
            echo $error->getMessage();
            $connex->rollBack();
            return false;
        } 
    }
}