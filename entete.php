<?php 
require("model/bdd.php");

spl_autoload_register(function ($class_name) {
    if (strstr($class_name, "Manager")) {
        include "model/managers/" . $class_name . '.class.php';
    } else {
        include "model/classes/" . $class_name . '.class.php';
    }
});