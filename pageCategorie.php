<?php 
require("entete.php");
if(isset($_GET["categorie"])){
    $idCateg=htmlspecialchars($_GET["categorie"]);
}
else{
   header("location:index.php");
}

$co=etablirCo();
$obArticleManager=new ArticleManager($co);
$lesArticles=$obArticleManager->fetchAllArticleByIdCategorie($idCateg);

$title="Shop : Les produits";
 ob_start();?>
 <h1>Les produits</h1>
 <div class="row row-cols-lg-4 row-cols-md-3 row-cols-sm-2">
 <?php foreach($lesArticles as $unArticle)
 {
     ?>
     <div class="card mx-auto my-2" style="width: 18rem;">
        <div class="card-body">
            <h5 class="card-title"><?=$unArticle->getNom()?></h5>
            <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6>
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            <a href="#" class="card-link">Card link</a>
            <a href="#" class="card-link">Another link</a>
        </div>
    </div>
     <?php
 }
 ?>
</div>
<?php 
$content=ob_get_clean();
require ("template.php");
