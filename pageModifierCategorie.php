<?php 
require ("entete.php");

if(isset($_GET["categorie"])){
    $idCategorie=htmlspecialchars($_GET["categorie"]);
}
else{
    header("location:index.php");
}

$co=etablirCo();
$obCategorieManager=new CategorieManager($co);
$uneCategorie=$obCategorieManager->fetchCategorieById($idCategorie);

$title="Shop : Formulaire de modification d'une Categorie";
ob_start()?>


<form class="col-lg-4 col-md-6 col-sm-8 mx-auto" action="" method="post">
<h1>Formulaire de modification</h1>
    <input readonly type="hidden" name="id" value="<?=$uneCategorie->getIdCategorie()?>">
    <div class="row">
        <label for="inputNom">Nom de la categorie :</label>
        <input class="form-control" required minlength="2" type="text" id="inputNom" name="nom" value="<?=$uneCategorie->getNom()?>">
    </div>
    <br>
    <button class="btn btn-success">Envoyer</button>
</form>

<?php
$content=ob_get_clean();
require("template.php");