<?php 
require("entete.php");


$co=etablirCo();
$obCatManager=new CategorieManager($co);
$lesCategories=$obCatManager->fetchAllCategorie();

$title="exemple title shop";
//Démare la temporisation
ob_start();
?>
<h1>Categories</h1>
<table class="table table-striped">
    <theader>
        <tr>
            <th>Id</th>
            <th>Nom</th>
            <th>Actions</th>
        </tr>
    </theader>
    <tbody>
    <?php foreach($lesCategories as $uneCategorie){?>
        <tr>
            <td><?=$uneCategorie->getIdCategorie()?></td>
            <td><?=$uneCategorie->getNom()?></td>
            <td>
                <a href="pageCategorie.php?categorie=<?=$uneCategorie->getIdCategorie()?>" class="btn btn-success">Consulter</a>
                <a href="pageModifierCategorie.php?categorie=<?=$uneCategorie->getIdCategorie()?>" class="btn btn-success">Modifier</a>
            </td>
        </tr>
    <?php }?>
    </tbody>
</table>
<?php
//recupere tout ce qui est temporisé est met le dans la variable content
$content=ob_get_clean();
require("template.php");


// --TESTS DE CATEGORIE MANAGER--
// $obCatManager=new CategorieManager($co);
// $lesCategories=$obCatManager->fetchAllCategorie();
// var_dump($lesCategories);

// $catParfum=$obCatManager->fetchCategorieById(2);
// var_dump($catParfum);

// // $resultatCreate=$obCatManager->createCategorie("vetement");
// // var_dump($resultatCreate);

// //$resultatUpdate=$obCatManager->updateCategorie("textile",4);
// $objetCatgorie4=new Categorie();
// $objetCatgorie4->setIdCategorie(4);
// $objetCatgorie4->setNom("Manteau");
// $resultatUpdate=$obCatManager->updateCategorie2($objetCatgorie4);
// var_dump($resultatUpdate);

// $resultatDelete=$obCatManager->deleteCategorie(1);
// var_dump($resultatDelete);

//--TESTS DE ARTICLEMANAGER --
// $obArticleManager= new ArticleManager($co);
// $lesArticles=$obArticleManager->fetchArticleByIdArticle(2);
// var_dump($lesArticles);
//$resultatCreate=$obArticleManager->createArticle("tedy","un ours en peluche",-15,true,false,3);
// $tedy=new Article();
// $tedy->hydrate("tedy","un ours en peluche",25,true,false,3);
// $tedy->setNom("tedy");
// $tedy->setDescription("un ours en peluche");
// $tedy->setPrixUnitaire(15);
// $tedy->setEstDisponible(true);
// $tedy->setEstFragile(true);
// $tedy->setIdCategorie(3);

// $resultatCreate=$obArticleManager->createArticle2($tedy);
// var_dump($resultatCreate);

// $tedy->setIdArticle(13);
// $tedy->setEstDisponible(false);
// $resultatUpdate=$obArticleManager->updateArticle($tedy);
// var_dump($resultatUpdate);

// $resultatDelete=$obArticleManager->deleteArticle(12);
// var_dump($resultatDelete);